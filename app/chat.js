Chat = new Object();
/*
 class Chat
 @startChatServer (server) : starting socket server by listeing argument server
 online : property, current amount of connections
*/

Chat.online = 0;
var list   = new Array();

Chat.startChatServer = (server, db) => {
  this.io = new require('socket.io').listen(server);
  console.log("Chat was started.");

  this.io.on('connection', (socket) => {
    this.online++;

    socket.on("outgoingMessage", (data) => {
      var self = this;

      db.Users.find({steamid : data.steamId}).toArray(function (err, list) {
        if (err || !list[0] || list[0].muted) return;
        data.rank = list[0].rank;
        data.steamid = list[0].steamid;
        self.io.sockets.emit("incomingMessage", data);
      });
    });

    socket.on("mute", (data) => {
      db.Users.update({steamid : data}, {$set : {muted : true}});
    });

    socket.on("unmute", (data) => {
      db.Users.update({steamid : data}, {$set : {muted : false}});
    });

    socket.on("clearClient", (data) => {
      this.io.sockets.emit("clearServer", data);
    });

    socket.on("identification", (data) => {
      if (!data.Steamid) return;
      list.push({
        Steamid  : data.Steamid,
        Socketid : socket.id
      });
    });

    socket.on('disconnect', () => {
      this.online--;

      if (!list) return;

      list.forEach( (item, index, arr) => {
        if (item.Socketid == socket.id) list.remove(index);
      });
    });

  }).bind(this);
};

module.exports.startServer = Chat.startChatServer;
module.exports.online      = Chat.online;
module.exports.io          = Chat.io;
module.exports.list        = list;

Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};