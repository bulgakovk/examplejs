const config      = require('../config/index.js');
const steamlogin  = require('steam-login');
const crypto      = require('crypto');

module.exports = function(app, db) {
  /*
  Home page
  */
  app.get('/', function(req,res) {
    res.render('index', {});
  });

  app.get('/settings', function(req, res) {
    res.render('settings', {});
  });

  app.get('/top', function(req, res) {
    res.render('top', {});
  });

  app.get('/history', function(req, res) {
    res.render('history', {});
  });

  app.get('/admincp', function(req, res) {
    res.render('admin', {});
  });
  /*
  Steam authenticate
  */

  app.use(steamlogin.middleware({
    realm: 'http://csGoBay.net' + '/',
    verify: 'http://csGoBay.net' + '/verify',
    apiKey: config.api_key}
  ));

  app.get('/login', steamlogin.authenticate(), function(req, res) {
    res.redirect('/');
  });

  app.get('/verify', steamlogin.verify(), function(req, res) {
    var user = req.user;
    res.cookie('Steamid', user.steamid);
    res.cookie('Avatar', user.avatar.medium);
    res.cookie('Username', user.username);
    var secret = config.csrf_token + user.steamid;
    var hash = crypto.createHmac('sha256', secret).digest('base64');
    res.cookie('csrf', hash);
    db.Users.find({steamid : user.steamid}).toArray(function(err, list) {
      if (err) return;
      if (list[0]) {
        if (list[0].tradeLink) res.cookie('tradeLink', list[0].tradeLink);
        if (list[0].rank) res.cookie('rank', list[0].rank);
        if (typeof list[0].winnings != undefined) res.cookie('all_time_profit', list[0].winnings/100);
      } else {
        db.Users.insert({ avatar: user.avatar.medium,
                          username: user.username,
                          steamid : user.steamid,
                          wins_amount : 0,
                          winnings    : 0,
                          wins_amount_month : 0,
                          winnings_month : 0 });
      }
      res.redirect('/');
    });
  });
}
