const secs_per_day   = 86400;
const secs_per_month = 2592000;
const timer_interval = 3600000;

function startTimer (db) {
	console.log("Timer was started.");
	setInterval( function () {
		db.Stats.find().toArray((err, list) => {
			if (err || !list[0]) return;
			var time_now = getTimer();

			if (time_now - list[0].last_time_day_record >= secs_per_day) {
				db.Stats.update({}, {$set : {last_time_day_record : time_now}});
				newDay(db);
			}  

			if (time_now - list[0].last_time_month_record >= secs_per_month) {
				db.Stats.update({}, {$set : {last_time_month_record : time_now}});
				newMonth(db);
			} 
		});
	}, timer_interval);
}

function newDay(db) {
	db.Stats.update({}, {
		$set : {
			games_today : 0,
			biggest_win_today : 0,
			items_raffled_today : 0
		}
	}, {multi : true});
}

function newMonth(db) {
	db.Users.update({wins_amount_month : {$ne : 0}}, {$set : {
		wins_amount_month : 0,
		winnings_month    : 0
	}}, {multi : true});
	db.Stats.find({}).toArray(function(err,res) {
		db.Admin.update({number : res[0].month % 12}, 
						{$set : {profit : 0}})
	}, function(err,result) {
		db.Stats.update({}, {$inc : {month : 1}});
	});
}

function getTimer () {
  var time = new Date().getTime();
  return time = Math.floor(time/1000);
}

module.exports = startTimer;