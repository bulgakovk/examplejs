const multer       = require('multer');
const upload       = multer();
const config       = require('../config/index.js');
const bodyParser   = require('body-parser');
const crypto       = require('crypto');
const md5          = require('md5');

module.exports = function (app, db, chat) {
  app.use(bodyParser.json());

  /*
  URL to add tradelink, available from 'settings' page.
  */
  app.post('/add_trade_link', upload.array(), function (req, res) {
      var tradeLink = req.body.tradeLink;
      var user = escape(req.cookies.Steamid);
      var hash_client = req.cookies.csrf;
      var secret = config.csrf_token + user;
      var hash_server = crypto.createHmac('sha256', secret).digest('base64');
      if (hash_client != hash_server) { return res.send("Log through Steam!"); }
      db.Users.update({steamid: user}, { $set: { tradeLink: tradeLink } } );
      res.cookie('tradeLink', tradeLink);
      res.send('OK');
  });

  /*
  Common game information, requesting when home ('/') page loads.
  */

  app.get('/game_info', function(req,res) {
    db.Games.find({isFinished : false}).toArray(function (err, list) {
      var response = new Object();
      if (err || !list[0]) {
        /*
        When something wrong with db or data.
        */
        response.itemsPrice    = 0;
        response.playersAmount = 0;
        res.send(response);
        return;
      }
      response.number     = list[0].number;
      response.itemsPrice = list[0].itemsPrice;

      if (list[0].players) {
        response.playersAmount = list[0].players.length;
        response.players       = list[0].players;
      } else response.playersAmount = 0;

      if (list[0].items) {
        response.items = list[0].items;
        response.itemsAmount = list[0].items.length;
      } else {
        response.items       = false;
        response.itemsAmount = 0;
      }
      response.hash   = list[0].hash;
      response.online = chat.online;
      res.send(response);
    });
  });

  app.get('/game_history', function (req,res) {
    db.Games.find({isFinished : true}, {sort : {number : -1}}).toArray(function(err, list) {
      if (err || !list[0]) { res.send([]); return; }
      res.send(list);
    });
  });

  app.get('/adminAPI', function (req, res) {
    db.Admin.find({}).toArray(function(err,list){
      if (err || !list[0]) {res.send([]); return;}
      res.send(list);
    });
  });

  app.get('/users', function (req, res) {
    db.Users.find({}).toArray(function(err, list) {
      if (err || !list[0]) { res.send([]); return; }
      res.send(list);
    });
  });
  app.get('/game_stat', function (req, res) {
    db.Stats.find().toArray(function (err, list) {
      if (err || !list[0]) return;
      res.send({
        games_today         : list[0].games_today,
        biggest_win_today   : list[0].biggest_win_today,
        items_raffled_today : list[0].items_raffled_today
      });
    });
  });

  app.get('/top_players', function (req, res) {
    db.Users.find({}, {limit : 10, sort : {winnings : -1}}).toArray(function (err, list) {
      db.Users.find({}, {limit : 10, sort : {winnings_month : -1}}).toArray(function(err,list2) {
        if (err || !list[0] || !list2[0]) {res.send([]); return;}
        res.send({
          total : list,
          month : list2
        });
      });
    });
  });
  /*
  Bot API
  */
  app.post('/api/v1/deposit', upload.array(), function (req, res) {
    console.log("Got deposit!");
    var resp = new Object();
    db.Games.find({isFinished : false}).toArray(function(err, list) {
      if (err || !list[0]) return;
      resp.itemsPrice    = list[0].itemsPrice;
      resp.players       = list[0].players;
      resp.items         = list[0].items;
      resp.playersAmount = list[0].players.length;

      if (resp.players.length == 2 && !timer_boolean_global && resp.items.length < 50) {
        console.log("STATUS : 1");
        resp.status = 1;
        timer_boolean_global = true;
        timer_global = setTimeout(chooseWinner, 91000, db, chat);
        var timer_local = setTimeout(function(){
          db.Games.update({isFinished : false}, {$set : {jackpotOn : true}});
          console.log("jackpotOn set as true!");
        }, 89000);
      }
      if (resp.items.length >= 50) {
        if (timer_global) clearInterval(timer_global);
        resp.status = 2;
        chooseWinner(db, chat);
      }
      chat.io.sockets.emit("new_deposit", resp);
    });
    res.send('OK');
  });
  
  app.post('/make_moderator', upload.array(), function (req, res) {
    var steamid = req.body.steamid;
    db.Users.update({steamid : steamid}, {$set : {rank : 'moderator'}});
    res.send("Updated");
  });

  app.post('/delete_moderator', upload.array(), function (req, res) {
    var steamid = req.body.steamid;
    db.Users.update({steamid : steamid}, {$set : {rank : ''}});
    res.send("Updated");
  });

  app.post('/unmute_user', upload.array(), function (req, res) {
    var steamid = req.body.steamid;
    db.Users.update({steamid : steamid}, {$set : {muted : false}});
    res.send("Updated");
  });
  
  app.post('/api/v1/confirmation', upload.array(), function (req, res) {
    if (!req.body.Steamid || !req.body.Items) return;
    chat.list.forEach(function(item) {
      if (item.Steamid == req.body.Steamid) {
        if (chat.io.sockets.connected[item.Socketid]) 
          chat.io.sockets.connected[item.Socketid].emit('confirmation', req.body);
      }
    });
    res.send("OK");
  });
}

/*
Some support functions
*/
var timer_boolean_global;
var timer_global;

function chooseWinner (db,chat) {
  timer_boolean_global = false;
  clearInterval(timer_global);
  console.log("choosing started");
  var boxesSum = 0;
  var boxesMin = 100;
  var boxesInd = 0;
  var minId;
  var winnerId;
  var players;
  var itemsPrice;
  var boxesArr = [];
  var boxesArrPointer = 0;
  var items;

  db.Games.find({isFinished : false}).toArray(function(err, list) {
    if (err || !list[0]) return;
    players    = list[0].players;
    itemsPrice = list[0].itemsPrice;
    items      = list[0].items;

    players.forEach(function(player,i,arr){
      player.chance = ((player.itemsPrice / itemsPrice)*100).toFixed(2);
      player.boxes  = Math.floor(player.chance);
      boxesSum += player.boxes;
      for (count = 0; count<player.boxes; count++){
        var obj = new Object();
        obj.steamId = player.steamId;
        obj.avatar  = unescape(player.avatar);
        obj.name    = unescape(player.name);
        obj.chance  = player.chance;
        boxesArr[boxesArrPointer] = obj;
        boxesArrPointer++;
      }
      if (player.boxes < boxesMin){
        boxesMin = player.boxes;
        boxesInd = i;
        minId    = player.steamId;
      }
    });

    if (boxesSum != 100){
      players.forEach(function(player, i , arr){
        if (player.steamId == minId){
          for (i = boxesSum; i<100; i++){
            var obj = new Object();
            obj.steamId = player.steamId;
            obj.avatar  = player.avatar;
            obj.name    = player.name;
            obj.chance  =  player.chance;
            boxesArr[i] = obj;
          }
        }
      });
    }

    var winner = selfRandom(0,99);
    boxesArr = shuffle(boxesArr);
    if (winner != 94){
      var temp = {};
      temp.avatar = boxesArr[94].avatar;
      temp.steamId = boxesArr[94].steamId;
      temp.name = boxesArr[94].name;
      boxesArr[94].steamId = boxesArr[winner].steamId;
      boxesArr[94].avatar = boxesArr[winner].avatar;
      boxesArr[94].name = boxesArr[winner].name;
      boxesArr[94].chance = boxesArr[winner].chance;
      boxesArr[winner] = temp;
    }
    var winnerId = boxesArr[94].steamId;
    
    var ticket = 6;
    var sum    = 0;
    for (var counter = 0; counter < list[0].items.length; counter++) {
      sum += list[0].items[counter].price;
      if (list[0].items[counter].ownerId == winnerId) {
        ticket = sum;
        break;
      }
    }
    var hash = md5(ticket);
    chat.io.sockets.emit("roulette", {boxesArr : boxesArr, itemsPrice : itemsPrice, ticket : ticket, hash : hash});
    db.Games.update({isFinished : false}, {$set : {result : boxesArr, winnerId : winnerId, realwinner : boxesArr[94]}}, function(err, result) {
      if (err) console.log(err);
      console.log(result);
      console.log("callback was called!");
    });
    setTimeout(startNewGame, 20000, db, chat, hash);
  });
}

function startNewGame (db, chat, hash) {
  var number;
  timer_boolean_global = false;
  db.Games.find({isFinished : false}).toArray(function(err, data){
    if (err || !data[0]) {
      console.log("CANT FIND LAST GAME");
      return;
    }
    number = data[0].number + 1;

    //Closing last game
    db.Games.update({isFinished : false}, {$set : {isFinished : true}});

    //Update stats
    db.Stats.find({}).toArray(function (err, list) {
      if (err || !list[0]) return;

      if (data[0].itemsPrice > list[0].biggest_win_today) {
        db.Stats.update({}, {
          $inc : {
            games_today         : 1,
            items_raffled_today : data[0].items.length,
            biggest_win_today   : data[0].itemsPrice
          },
        });
      } else {
        db.Stats.update({}, {
          $inc : {
            games_today         : 1,
            items_raffled_today : data[0].items.length 
          },
        });
      }     
    });

    var profit = 0;
    data[0].items.forEach(function(item){
      if (item.ownerId != data[0].realwinner.steamId) profit += item.price;
    });

    //Update personal stats
    db.Users.update({steamid : data[0].realwinner.steamId},
    {$inc : {wins_amount : 1, wins_amount_month : 1, 
            winnings : profit, winnings_month : profit
    }});
    //End of updating stats
    db.Games.insert({
      isFinished : false,
      number     : number,
      itemsPrice : 0,
      departed   : false,
      jackpotOn  : false,
      time       : getTimer(),
      hash       : hash
    });

    chat.io.sockets.emit("startNewGame", number);
  });
}

function selfRandom(min, max)
{
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function shuffle(arr) {
  for (var i = arr.length - 1; i > 0; i--) {
    if (i!=94){
      var num = Math.floor(Math.random() * (i + 1));
      while (num == 94) num = Math.floor(Math.random() * (i + 1));
      var d = arr[num];
      arr[num] = arr[i];
      arr[i] = d;
    }
  }
  return arr;
}

function getTimer () {
  var time = new Date().getTime();
  return time = Math.floor(time/1000);
}

function checkList(obj) {
  var res = findObj(obj);
  console.log("res here is " + res);
  if (res == -1){
    arr_chat.push(obj);
    return true;
  } else if (arr_chat.length != 0 && getTimer() - arr_chat[res].time >= 30) {
    arr_chat[res].time = getTimer();
    return true;
  } else return false;
}

function findObj(obj) {
  var res = -1;
  for (var i = 0; i < arr_chat.length; i++){
    if (arr_chat[i].id == obj.id) {
      res = i;
      break;
    }
  }

  return res;
}
