const MongoClient = require('mongodb').MongoClient;
const config      = require('../config/index.js');
const Database    = require('./classes/Database.js')

const connect = new Promise(function(resolve, reject) {
  MongoClient.connect(config.host_db + config.name_db, function (err, res) {
    if (err) throw new Error(err);
    const db = new Database(res.collection('Users'),
                            res.collection('Chat'),
                            res.collection('Games'),
                            res.collection('Stats'),
                            res.collection('Admin'));
    // console.log("Connection with database was established.");
    resolve(db);
  });
});

module.exports = connect;
