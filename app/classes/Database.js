'use strict';

class Database {
  constructor (Users, Chat, Games, Stats, Admin) {
    this.Users = Users;
    this.Chat  = Chat;
    this.Games = Games;
    this.Stats = Stats;
    this.Admin = Admin;
  }
}

module.exports = Database;
