var scroll = 
function(){
    $(".chat").niceScroll({cursorcolor:"rgba(255,255,255,0.3)", cursorborder:"0px solid #fff", cursoropacitymax:"0.75", zindex: 1000});
    $(".players").niceScroll({cursorcolor:"rgba(255,255,255,0.3)", cursorborder:"0px solid #fff", cursoropacitymax:"0.75", zindex: 1000});
    $(".history-pots").niceScroll({cursorcolor:"rgba(255,255,255,0.3)", cursorborder:"0px solid #fff", cursoropacitymax:"0.75", zindex: 1000});
};

scroll();

var scrollChat = function() {
    $(".chat").niceScroll({cursorcolor:"#D0D0D0", cursorborder:"0px solid #fff", cursoropacitymax:"0.75", zindex: 1000}).resize();
    setTimeout( function() {
        $('.chat').scrollTop($('.chat').prop('scrollHeight'));
    }, 500);
}

var tooltip = function () {
	$('.tooltip').tooltipster();
}