app.factory('socket', function($rootScope){
	var socket = io.connect('http://5.135.169.233:3000');

	return {
		on : function(eventName, callback) {
			socket.on(eventName, function () {
				var args = arguments;
				$rootScope.$apply(function () {
		          callback.apply(socket, args);
        });
			});
		},

		emit : function(eventName, data, callback) {
			socket.emit(eventName, data, function () {
				var args = arguments;
				$rootScope.$apply(function () {
					if (callback) {
						callback.call(socket, args);
					}
				})
			})
		}
	}
});
