var timer_global;
var players;


function selfRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

app.controller('chatCtrl', function ($scope, socket, $cookies) {
  $scope.messages = [];

  $cookies.put("messages_amount", 0);
  $cookies.put("flood", false);
  var time = 30;

  function increaseMessages () {
    var messages = parseInt($cookies.get("messages_amount"), 10);
    if (messages == 4) {
      $cookies.put("flood", true);
      return;
    }
    $cookies.put("messages_amount", messages + 1);
  }

  setInterval(function () {
    $cookies.put("flood", false);
    $cookies.put("messages_amount", 0);
    time = 30;
  }, 30000);

  setInterval(function () {
    time--;
  }, 1000);
  //Listening to new messages
	socket.on('incomingMessage', function (data) {
		var obj = new Object();
		obj.avatar   = data.avatar;
		obj.username = data.username;
    obj.rank     = data.rank;
    obj.steamId  = data.steamId;
		var str      = data.value.split('');
    obj.value    = "";
    str.forEach(function(ch){
      if (ch.charCodeAt(0) == 10) obj.value += "<br>"
      else obj.value += ch;
    });
		$scope.messages.push(obj);
    scrollChat();
  });

  socket.on('clearServer', function (data) {
    var arr = [];
    $scope.messages.forEach(function(message, ind, array) {
      if (message.steamId != data) arr.push(message);
      else {
        message.value = "&ltDeleted&gt";
        arr.push(message);
      }
    });
    $scope.messages = arr;
  });
  /*
  Handling events
  */

  $scope.sendMessage = function () {
    var avatar   = $cookies.get("Avatar");
    var username = $cookies.get("Username");
    var steamid  = $cookies.get("Steamid");

    if (username == undefined) {
      swal("Oopss...", "Need to login!", "error");
      return;
    }

    var data = new Object();
    
    if (!$scope.message) {
      swal("Oopss...", "You can't send empty message!", "error");
      return;
    }

    if ($cookies.get("flood") == "true") {
      swal("Oopss...", "You're sending messages too fast! Please wait " + (time) + " seconds!", "error");
      return;
    }

    data.value    = $scope.message;
    data.username = username;
    data.avatar   = avatar;
    data.steamId  = steamid;
    socket.emit("outgoingMessage", data);
    $scope.message = "";
    increaseMessages();
  }

  $scope.keypress = function (event) {
    if (event.charCode != 13) return;
    if (event.charCode == 13 && event.shiftKey) return;
    event.preventDefault();
    var avatar   = $cookies.get("Avatar");
    var username = $cookies.get("Username");
    var steamid  = $cookies.get("Steamid");

    if (username == undefined) {
      swal("Oopss...", "Need to login!", "error");
      return;
    }

    if (!$scope.message) {
      swal("Oopss...", "You can't send empty message!", "error");
      return;
    }

    if ($cookies.get("flood") == "true") {
      swal("Oopss...", "You're sending messages too fast! Please wait " + (time) + " seconds!", "error");
      return;
    }

    var data = new Object();
    data.value    = $scope.message;
    data.username = username;
    data.avatar   = avatar;
    data.steamId  = steamid;
    socket.emit("outgoingMessage", data);
    $scope.message = "";
    increaseMessages();
  }

  $scope.isModerator = function () {
    return $cookies.get('rank') == "moderator";
  }

  $scope.muteUser = function (event) {
    event.preventDefault();
    socket.emit('mute', event.target.id);
  }

  $scope.unmuteUser = function (event) {
    event.preventDefault();
    socket.emit('unmute', event.target.id);
  }

  $scope.clear = function(event) {
    event.preventDefault();
    var steamid = event.target.id;
    socket.emit('clearClient', steamid);
  }

});

app.controller('menuCtrl', function ($scope, $cookies, socket) {
  $scope.isLogged = function () {
    return $cookies.get("Avatar") != undefined;
  }

  $scope.loginWithSteam = function () {
    document.location.href = "/login";
  }

  $scope.getAvatar = function () {
    return unescape($cookies.get("Avatar"));
  }

  $scope.getUsername = function () {
    return unescape($cookies.get("Username"));
  }

  $scope.openSettings = function () {
    document.location.href = "/settings";
  }

  $scope.logout = function () {
    $cookies.remove("Avatar");
    $cookies.remove("Username");
    $cookies.remove("tradeLink");
    $cookies.remove("Steamid");
    $cookies.remove("csrf");
    $cookies.remove("rank");
    if (document.location.href.indexOf("settings") != -1) {
      document.location.href = "/";
    }
  }

  socket.emit('identification', {Steamid : $cookies.get("Steamid")} );
});

app.controller('settingsCtrl', function ($scope, $http, $cookies) {
  $scope.saved = false;
  if ($cookies.get('tradeLink')) $scope.tradeLink = unescape($cookies.get('tradeLink'));
  if ($cookies.get('all_time_profit')) $scope.all_time_profit = unescape($cookies.get('all_time_profit')) + "$";
  $scope.saveSettings = function () {
    var regexp = /steamcommunity\.com\/tradeoffer\/new\/\?partner=[0-9]*&token=[a-zA-Z0-9_-]*/i;
    if ($scope.tradeLink == undefined || $scope.tradeLink.search(regexp) == -1){
      alert("Invalid link!");
      return;
    }

    $http.post('/add_trade_link', { tradeLink : $scope.tradeLink } ).then(callback);

    function callback (resp) {
      $scope.saved = true;
    }
  }

  $scope.savedSuccess = function() {
    return $scope.saved;
  }
});

app.controller('contentCtrl', function ($scope, $cookies, $http, socket) {
  /*
  Checking start timer or not (last game could be not finished)
  */
  if ($cookies.get('time')) {
    if (getTime() - $cookies.get('time') <= 90) {
      var diff = getTime() - $cookies.get('time');
      timer(90 - diff);
    }
  }

  /*
  Some default values for this controller
  */
  $scope.secs = "30";
  $scope.mins = "01";
  $scope.showRoulette = false;
  $scope.hash = "liuhadc92y435rugsrfy3u9";
  /*
  Getting common info about game etc.
  */
  $http.get('/game_info').then(callback);
  $http.get('/game_history').then(historyCallback);
  $http.get('/game_stat').then(statCallback);

  function statCallback (resp) {
    $scope.games_today         = resp.data.games_today;
    $scope.biggest_win_today   = resp.data.biggest_win_today;
    $scope.items_raffled_today = resp.data.items_raffled_today; 
  }

  function historyCallback (resp) {
    var data;
    resp.data ? data = resp.data : data = resp;
    if (data) {
      var lastGame = data[0].result[94];
      $scope.lastUsername = lastGame.name;
      $scope.lastAvatar   = lastGame.avatar;
      $scope.lastChance   = lastGame.chance;
      $scope.lastWinnings = data[0].itemsPrice;
    }
  }

  function callback (resp) {
    var data;
    resp.data ? data = resp.data : data = resp;
    /*
    Main information about game
    */

    $scope.itemsPrice    = data.itemsPrice;
    if (data.number) $scope.number   = data.number;
    data.items  ? $scope.itemsAmount = data.items.length
                : $scope.itemsAmount = 0;
    if (data.online) $scope.online = data.online;
    if (data.hash) $scope.hash = data.hash; else $scope.hash = "LIUHADC92Y435RUGSRFY3U9"; 
    if (!data.items) return;
		//we can parse then cause of we're sure that we have players and items
		$scope.skins   = [];
		$scope.players = [];

		data.items.forEach(function(item) {
			if (item.image.indexOf('http') == -1) {
				item.image = 'https://steamcommunity-a.akamaihd.net/economy/image/' + item.image;
			}
			item.price = item.price /100 ;
			$scope.skins.push(item);
		});

    $scope.skins.reverse();

    var players_arr = [];
		data.players.forEach(function(player) {
			player.avatar = unescape(player.avatar);
			player.chance = ((player.itemsPrice / data.itemsPrice)*100).toFixed(2);
      player.name   = unescape(player.name);
			players_arr.push(player);
		});

    players_arr.sort(function(a1,a2) {
      return a1.chance < a2.chance;
    });

    $scope.players = players_arr;
    setTimeout(scroll, 500);
  }

  /*
  Defining socket events
  */

  socket.on("new_deposit", function (resp) {
		if (resp.status == 1) {
			timer();
		} else if (resp.status == 2) {
      if (timer_global) clearInterval(timer_global);
    }
    //just refresh all info
    callback(resp);
	});

	socket.on('roulette', function(resp_players) {
    var elem = document.getElementsByClassName("r-images")[0];
    var img = "";
    var players = resp_players.boxesArr;

    for (i=0; i<100; i++){
        img = unescape(players[i].avatar);
        elem.innerHTML += "<img src=" + img + ">";
    }

    $scope.showRoulette = true;
    var deviation = 7920;
    deviation = "-=" + deviation.toString();
    $('.r-images').animate({left: deviation}, 12000, 'easeOutQuint');

    var notification = setTimeout( function () {
      $scope.winner = players[94].name;
      $scope.ticket = resp_players.ticket;
      $scope.lastUsername = players[94].name;
      $scope.lastChance   = players[94].chance;
      $scope.lastWinnings = resp_players.itemsPrice;
      $scope.lastAvatar   = players[94].avatar;
      $scope.$apply();
      //Доделать тикет
    }, 13000);

    var time = setTimeout(function () {
        $scope.showRoulette = false;
        $('.r-images').html("");
        $scope.hash = resp_players.hash;
        clearInterval(time);
    }, 18000);
  });

  socket.on('startNewGame', function(number) {
    $scope.number = number;
    $scope.itemsPrice = 0;
    $scope.itemsAmount = 0;
    $scope.skins = [];
    $scope.players = [];
    $scope.mins = "01";
    $scope.secs = "30";
    $scope.winner = "";
    $scope.ticket = "";
  });

  $scope.isLogged = function () {
    return $cookies.get("Avatar") != undefined;
  }

  $scope.depositRedirect = function (event) {
    if ($cookies.get('tradeLink') == undefined) {
      swal("Oopss...", "Need to save trade link!", "error");
      return;
    }
    // window.open("https://steamcommunity.com/tradeoffer/new/?partner=104220876&token=QODu_azB");
    window.open("https://steamcommunity.com/tradeoffer/new/?partner=242338162&token=FKPb9vYC");
    event.preventDefault();
  }

  $scope.getTickets = function (price) {
    var n = price * 100;
    return n.toFixed()
  }

  $scope.isRouletteStarted = function () {
    return $scope.showRoulette;
  }
  /*
  Support functions
  */

  function timer(a) {
    var time;
    if (!a) {
      time = 91;
      $cookies.put('time', getTime());
    }
    else time = a;
    timer_global = setInterval(function() {
        time--;
        if (time < 0) { 
          clearInterval(timer_global);
          return;
        }

        $scope.mins = "0" + Math.floor(time/60);
        if (time % 60 < 10) {
            $scope.secs = "0" + time % 60;
        } else $scope.secs = time % 60;
        $scope.$apply();
    }, 1000);
  }

  function getTime () {
    var time = new Date().getTime();
    return time = Math.floor(time/1000);
  }

});

app.controller('historyCtrl', function ($scope, $http) {
  $http.get('/game_history').then(successCallback);
  var players;
  var counter = 2;

  function successCallback (resp) {
    var data;
    resp.data ? data = resp.data : data = resp;
    data.forEach(function (game) {
      
      game.players.forEach(function(item) {
        item.chance = $scope.chance(item.itemsPrice, game.itemsPrice);
      });

      var temp = game.players;
      temp.sort(function(a1,a2){
        return a1.chance < a2.chance;
      });
      game.players = temp;

      game.items.forEach(function (item) {
        if (item.image.indexOf('http') == -1) {
          item.image = 'https://steamcommunity-a.akamaihd.net/economy/image/' + item.image;
        }
      });
    });

    players = data;
    $scope.games = players.slice(0,5);
    setTimeout(tooltip, 500);
  }

  $scope.chance = function(a, b) {
    return ((a/ b)*100).toFixed(2);
  }

  $scope.showMore = function(event) {
    $scope.games = players.slice(0, 5*counter++);
    event.preventDefault();
  }
});

app.controller('confirmationCtrl', function ($scope, socket, $cookies) {
  var socketBot = io.connect('http://5.135.169.233:3001');

  socket.on('confirmation', function(data) {
    openPopup();

    data.Items.forEach(function(item) {
      if (item.image.indexOf('http') == -1) {
        item.image = 'https://steamcommunity-a.akamaihd.net/economy/image/' + item.image;
      }
    });

    $scope.data = {
      totalCost : data.totalCost,
      Items     : data.Items,
      Steamid   : data.Steamid,
      Tradeid   : data.Tradeid
    };

  });

  socketBot.on('notificateError', function(data) {
    if ($cookies.get('Steamid') == data) {
       swal("Oopss...", "Your offer doesn't have minimal requires to be accepted, please check our rules page to understand why!", "error");
    }
  });

  $scope.confirmTrade = function (event) {
    var data = {
      Steamid : $cookies.get("Steamid"),
      csrf    : $cookies.get("csrf"),
      Tradeid : $scope.data.Tradeid
    };
    event.preventDefault();
    socketBot.emit('confirmTrade', data);
  }

  $scope.declineTrade = function (event) {
    var data = {
      Steamid : $cookies.get("Steamid"),
      csrf    : $cookies.get("csrf"),
      Tradeid : $scope.data.Tradeid
    };
    event.preventDefault();
    socketBot.emit('declineTrade', data);
  }

});

app.controller('topCtrl', function ($scope, socket, $cookies, $http) {
  $scope.data = {
    players       : [],
    players_total : [],
    players_month : [],
    all_times     : true,
    month         : false  
  };

  $http.get('/top_players').then(callback);
  
  function callback (resp) {
    $scope.data.players_total = resp.data.total;
    $scope.data.players_month = resp.data.month;
    $scope.data.players = resp.data.total;
  }

  $scope.getImage = function (index) {
    if (index == 0) return "images/1st.png"
    else if (index == 1) return "images/2nd.png";
    return "images/3rd.png";
  }

  $scope.changeMode = function (event) {
    if (event.currentTarget.innerText == "THIS MONTH") {
      if ($scope.data.all_times && !$scope.data.month) {
        $scope.data.all_times = false;
        $scope.data.month     = true;
        $scope.data.players   = $scope.data.players_month;
      } 
    } else {
      if (!$scope.data.all_times && $scope.data.month) {
        $scope.data.all_times = true;
        $scope.data.month     = false;
        $scope.data.players   = $scope.data.players_total;
      }
    }
  }
});

app.controller('adminCtrl', function($scope, $http){
  $http.get('/adminAPI').then(callback);
  $http.get('/users').then(usersCallback);
  $http.get('/game_history').then(gameCallback);

  function callback(resp) {
    var data = resp.data;
    var max = 0;
    for(i=0;i<12;i++) if (data[i].profit > max) max = data[i].profit;
    max--;
    $scope.data = data;
    $scope.max = max;
  }

  function usersCallback(resp) {
    var data = resp.data;
    $scope.users = data;
  }

  function gameCallback(resp) {
    var data = resp.data;
    $scope.games = data;
  }

  $scope.page = "stats";

  $scope.changeUsers = function () {
    if ($scope.page != "users") $scope.page = "users";
  }

  $scope.changeStats = function () {
    if ($scope.page != "stats") $scope.page = "stats";
  }

  $scope.changeGames = function() {
    if ($scope.page != "games") $scope.page = "games";
  }

  $scope.getColorStats = function () {
    if ($scope.page == 'stats') return "#1e2528";
  }

  $scope.getColorUsers = function () {
    if ($scope.page == 'users') return "#1e2528";
  }

  $scope.makeModerator = function(event) {
    var steamid = event.currentTarget.id;
    $http.post('/make_moderator', {steamid : steamid}).then(function(resp) {
      alert("Done!");
    });
    event.preventDefault();
  }

  $scope.deleteModerator = function(event) {
    var steamid = event.currentTarget.id;
    $http.post('/delete_moderator', {steamid : steamid}).then(function(resp) {
      alert("Done!");
    });
    event.preventDefault();
  }

  $scope.unmuteUser = function(event) {
    event.preventDefault();
    var steamid = event.currentTarget.id;
    $http.post("/unmute_user", {steamid : steamid}).then(function(resp) {
      alert("Unmuted!");
    });
  }
});

function openPopup () {
  $(".close").click(function(){$("#lean_overlay").fadeOut(200);$("#deposit").css({"display":"none"})});
  $(".confirm").click(function(){$("#lean_overlay").fadeOut(200);$("#deposit").css({"display":"none"})})  
  $("#lean_overlay").css({"display":"block",opacity:0});$("#lean_overlay").fadeTo(200,"0.85"); 
  $("#deposit").css({"display":"block","position":"fixed","opacity":0,"z-index":11000,"left":50+"%","margin-left":-($("#deposit").outerWidth()/2)+"px","top":"100px"});$("#deposit").fadeTo(200,1);
}

function isLetter(str) {
  if (str.search(/[a-z]/i) || str.search(/[0-9]/i) || str.search(/[A-Z]/i)) {
    return str.length === 1;
  }
  return false;
}

Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
}

function getTime () {
  var time = new Date().getTime();
  return time = Math.floor(time/1000);
}