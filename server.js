const express      = require('express');
const app          = express();
const server       = require('http').createServer(app);
const chat         = require('./app/chat.js');
const cookieParser = require('cookie-parser');
const timer        = require('./app/timer.js');

/*
  Starting server
*/

server.listen("3000");

/*
  App configs
*/

app.use(express.static(__dirname + '/public'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(require('express-session')({ resave: false, saveUninitialized: false, secret: 'a2131sec2Fet' }));
app.use(cookieParser('my secret here'));

/*
  Define routers for app
*/

const mongoConnect = require('./app/mongo.js');
// timer();

mongoConnect
.then( (db) => {
	require('./app/routers.js')(app, db);
	require('./app/api.js')(app, db, chat);
	timer(db);
	chat.startServer(server, db);
}, (error) => {
	console.log(error);
});